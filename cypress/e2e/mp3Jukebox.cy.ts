//
// File: mp3Jukebox.cy.ts
// Auth: Martin Burolla
// Date: 5/29/2023
// Desc:
//

/// <reference types="cypress" />

describe('MP3 Jukebox', () => {
    beforeEach(() => {
      cy.visit(Cypress.env('s3-bucket'))
    })

    it("Opens sidedrawer", () => {
      //cy.getItemById("D:001").click() // TODO: TypeScript project configuration.
      cy.get('[data-tid="D:001"]').click()
      cy.get("body").should("have.class", "modal-open")
      cy.get('[data-tid="B:003"]').click()
      cy.get("table").should("have.class", "foldersTable")
    })
})
