import { add } from "../../src/add"

describe('Add unit test', () => {
    it("adds two numbers", () => {
      let r = add(1,1)
      expect(r).to.equal(2)
    })
})