# TypeScript and Cypress
Testing with TypeScript and Cypress.

- [Intro](https://docs.cypress.io/guides/core-concepts/introduction-to-cypress)
- [EXAMPLES](https://example.cypress.io/)

Videos:
- https://docs.cypress.io/examples/tutorials
- https://www.youtube.com/cypress_io

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run TypeScript compiler: `tsc --watch`
- Open cypress: `npx cypress open --e2e`
- Run tests: `npm test`

# Examples
- [Getting Started](./cypress/e2e/1-getting-started/todo.cy.js)
- [Advanced](./cypress/e2e/2-advanced-examples/)

# Setup
- https://ganeshsirsi.medium.com/how-to-set-up-cypress-and-typescript-end-to-end-test-automation-framework-from-scratch-step-by-step-b4f887e0646
- `npx cypress open` Click on E2E testing
- https://docs.cypress.io/guides/end-to-end-testing/writing-your-first-end-to-end-test
- Must install TypeScript as a dev dependency
- Must modifiy tsconfig.json

# Commands
# Queries
- [get](https://docs.cypress.io/api/commands/get)
# Assertions
- [should](https://docs.cypress.io/api/commands/should)
# Actions
- [click](https://docs.cypress.io/api/commands/click)
# Other
- TBD

# ProTip: Add this to help VS Code:
/// <reference types="cypress" />

# Custom Commands
https://docs.cypress.io/api/cypress-api/custom-commands

# TypeScript Support
https://docs.cypress.io/guides/tooling/typescript-support
